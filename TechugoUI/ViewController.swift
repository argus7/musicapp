//
//  ViewController.swift
//  TechugoUI
//
//  Created by Ravi Ranjan on 19/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var soundImage: UIImageView!
    
    @IBOutlet var bottomView: UIView!
    @IBOutlet var musicControllerView: UIView!
    @IBOutlet var musicControllerCentreView: UIView!
    
    
    @IBOutlet var playButton: UIView!
    @IBOutlet var forwardButton: UIView!
    @IBOutlet var backButton: UIView!
    @IBOutlet var musicListButton: UIView!
    @IBOutlet var musicSlider: UISlider!
    
    @IBOutlet var infoButton: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.configView()
    }
    
    fileprivate func configView(){
        self.soundImage.layer.cornerRadius = self.soundImage.layer.frame.width / 2
   
    }
    
    
}

